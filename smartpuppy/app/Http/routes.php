<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware'=>'cors'], function(){
    //Api's Routes here
    Route::post('/api/authenticate', [
        'uses' => 'ApiAuthController@authenticate',
        'as' => 'apiAuthenticate'
    ]);

    Route::get('/api/test_token', [
        'uses' => 'ApiAuthController@test',
        'as' => 'apiTest'
    ]);
    Route::post('api/send', [
        'uses' => 'EmailController@postSend',
        'as' => 'apiSendEmail'
    ]);
    Route::post('api/resetpassword', [
        'uses' => 'EmailController@postResetPassword',
        'as' => 'apiResetPassword'
    ]);
    Route::post('api/addcode', [
        'uses' => 'ActivationController@postAddCode',
        'as' => 'apiAddCode'
    ]);

    Route::post('api/createaccount', [
        'uses' => 'ApiUserController@postCreateAccount',
        'as' => 'apiCreateAccount'
    ]);
    Route::post('api/setaccount', [
        'uses' => 'ApiUserController@postSetAccount',
        'as' => 'apiSetAccount'
    ]);
    Route::post('api/addtag', [
        'uses' => 'TagController@postAddTag',
        'as' => 'apiAddTag'
    ]);
    Route::post('api/activatetag', [
        'uses' => 'TagController@postActivateTag',
        'as' => 'apiActivateTag'
    ]);
    Route::post('api/adddeclaretag', [
        'uses' => 'TagController@postAdddeclareTag',
        'as' => 'apiAdddeclareTag'
    ]);
    Route::post('api/removedeclaretag', [
        'uses' => 'TagController@postRemovedeclareTag',
        'as' => 'apiRemovedeclareTag'
    ]);
    Route::post('api/checktag', [
        'uses' => 'TagController@postCheckTag',
        'as' => 'apiCheckTag'
    ]);
    Route::post('api/localizetag', [
        'uses' => 'TagController@postLocalizeTag',
        'as' => 'apiLocalizeTag'
    ]);
    Route::post('api/edittag', [
        'uses' => 'TagController@postEditTag',
        'as' => 'apiEditTag'
    ]);
    Route::post('api/deletetag', [
        'uses' => 'TagController@postDeleteTag',
        'as' => 'apiDeleteTag'
    ]);
});