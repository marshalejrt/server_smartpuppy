<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiUserController extends Controller
{
    //
    public function postCreateAccount(Request $request)
    {
        $request["role_id"]=2;

        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|email|min:4|max:62'
        ]);
        if ($validator->fails()) {
            return response()->json(['result'=>2,'errors' => $validator->errors()]);
        }


         $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users,email|email|min:4|max:62',
            'password' => 'required|min:6|max:20|confirmed',
            'firstname' => 'required|min:3|max:50',
            'lastname' => 'required|min:3|max:50',
            'role_id' => 'required|exists:roles,id',
            'version' => 'required'
        ]);

        if ($validator->fails()) {
                return response()->json(['result'=>1,'errors' => $validator->errors()]);
        }
        $request["password"] = bcrypt($request["password"]);
        $token = null;
        $user = User::create($request->only([
            'email',
            'password',
            'firstname',
            'lastname',
            'role_id',
            'version'
        ]));
        $user1 = User::find($user->id);
        if($token=JWTAuth::fromUser($user1)){
            return response()->json(['result' => 0,'token'=>$token, 'firstname'=>$user['firstname'],'lastname'=>$user['lastname']]);
        }else{
            return response()->json(compact('token'));
        }
        #$creadentials=array(['email'=>$user->email, 'password'=>$user->password,'version'=>$user->version]);
    }

    public function postSetAccount(Request $request)
    {
        JWTAuth::parseToken();
        $user_token = JWTAuth::parseToken()->authenticate();

            $validator = Validator::make($request->all(), [
                'email' => 'required|email|min:4|max:62|unique:users,id,'.$user_token->id,
                'password' => 'required|min:6|max:20|confirmed',
                'firstname' => 'required|min:3|max:50',
                'lastname' => 'required|min:3|max:50'
            ]);

            if ($validator->fails()) {
                return response()->json(['result'=>2,'errors' => $validator->errors()]);
            }
            if (!$user = User::find($user_token->id)){
                return response()->json(['result'=>2,'errors' => 'User not found']);
            }
            $user->email = $request["email"];
            $user->password = bcrypt($request["password"]);
            $user->firstname = $request["firstname"];
            $user->lastname = $request["lastname"];
            $user->update();
            $token = JWTAuth::refresh();
            return response()->json(['result' =>0, 'token' => $token,'user' => $user]);

    }
}
