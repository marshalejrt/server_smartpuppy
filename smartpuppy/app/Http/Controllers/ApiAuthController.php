<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
class ApiAuthController extends Controller
{
    //
    public function authenticate(Request $request)
    {
        $creadentials = $request->only('email', 'password','version');
        $token = null;

        try {
            if (!$token = JWTAuth::attempt($creadentials)) {

                if (!JWTAuth::attempt(['email' => $creadentials['email'],'password' => $creadentials['password']])) {
                    if(!JWTAuth::attempt(['password' => $creadentials['password']])){
                        return response()->json(['result' => 2], 401);
                    }
                    return response()->json(['result' => 1], 401);
                }
                if(!JWTAuth::attempt(['version' => $creadentials['version']])){
                    return response()->json(['result' => 3], 401);
                }


                return response()->json($creadentials, 500);
            }
        } catch (JWTException $e) {
            return response()->json(['mensaje' => $e->getMessage()], 500);
        }
        $user = JWTAuth::toUser($token);
        $tags = $user->tags;
        $list = array();
        foreach($tags as $k=>$v){
            $list[$k]=array();
            $list[$k]["serial"] = $v["serial"];
            $list[$k]["name"] = $v["name"];
        }

        //        return response()->json(compact('token', 'user'));
        return response()->json(['result' => 0,'token'=>$token,'firstname'=>$user['firstname'],'lastname'=>$user['lastname'],'tags'=>$list], 500);
    }

    public function test(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::refresh();
        return response()->json(compact('user', 'token'));
    }
}
