<?php

namespace App\Http\Controllers;

use App\Activation;
use App\Tag;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class TagController extends Controller
{
    public function postAddTag(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();
        $validator = Validator::make($request->all(), [
            'serial' => 'required|min:4|max:62'//,
            //'name'   => 'required|min:3|max:50'
        ]);

        if ($validator->fails()) {
            return response()->json(['result'=>'2','errors' => $validator->errors()]);
        }
        $request['latitude']='0.0';
        $request['name']='';
        $request['longitude']='0.0';
        $request['user_id']=$user['id'];
        $request['activate']='0';//i'ts to define
        $request['code']='';
        if ($tag = Tag::where('serial',$request["serial"])->first()) {
            if($tag->user_id!=1){
                return response()->json(['result'=>'2','errors' => 'Tag asigned to another user']);
            }
            $tag->serial= $request['serial'];
            $tag->name= $request['name'];
            $tag->longitude= $request['longitude'];
            $tag->latitude= $request['latitude'];
            $tag->user_id= $request['user_id'];
            $tag->code= $request['code'];
            $tag->update();

        }else{
            $tag = Tag::create($request->only([
                'serial',
                'name',
                'latitude',
                'longitude',
                'user_id',
                'activate',
                'code'
            ]));
        }

       return response()->json(['result' => 0,'activation'=>(($tag->activate)-1)*-1], 500);

    }

    public function postEditTag(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();
        $validator = Validator::make($request->all(), [
            'serial' => 'required|min:4|max:62',
            'name'   => 'required|min:3|max:15'
        ]);

        if ($validator->fails()) {
            return response()->json(['result'=>'2','errors' => $validator->errors()]);
        }



        //if (!$tag = Tag::find($request["serial"])) {
        if (!$tag = Tag::where('serial',$request["serial"])->first()) {
            //$token = JWTAuth::refresh();
            //, 'token' => $token
            return response()->json(['result' => '2', 'errors' => 'Tag not found']);
        }else{
            $tag->name = $request["name"];
            $tag->update();
//            return response()->json(['result'=>'0','tag' => $tag]);
            return response()->json(['result'=>0]);
        }
    }

    public function postActivateTag(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();
        $validator = Validator::make($request->all(), [
            'serial' => 'required|min:4|max:62',
            'code'   => 'required|min:3|max:50'
        ]);

        if ($validator->fails()) {
            return response()->json(['result'=>'2','errors' => $validator->errors()]);
        }



        //if (!$tag = Tag::find($request["serial"])) {
        if (!$tag = Tag::where('serial',$request["serial"])->first()) {
            //$token = JWTAuth::refresh();
            //, 'token' => $token
            return response()->json(['result' => '2', 'errors' => 'Tag not found']);
        }

        if ($tag['user_id']!=$user->id){
            return response()->json(['result' => '2', 'errors' => 'Tag not current user']);
        }

        if (!$activation = Activation::where('code',$request['code'])->first()) {
            return response()->json(['result' => '2', 'errors' => 'Not found The Activation code']);
        }else{
            if($activation->expire==1){
                return response()->json(['result' => '2', 'errors' => 'The Activation code has expired']);
            }
        }

        $activation->expire=1;
        $activation->serial=$request["serial"];
        $activation->update();

        $tag->activate = 1;
        $tag->update();
//        return response()->json(['result'=>'0','tag' => $tag]);
        return response()->json(['result'=>'0']);


    }

    public function postAdddeclareTag(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();
        $validator = Validator::make($request->all(), [
            'serial' => 'required|min:4|max:62'
        ]);

        if ($validator->fails()) {
            return response()->json(['result'=>'2','errors' => $validator->errors()]);
        }

        if (!$tag = Tag::where('serial',$request["serial"])->first()) {
            return response()->json(['result' => '2', 'errors' => 'Tag not found']);
        }
        if (!$tag['user_id']==$user['id']){
            return response()->json(['result' => '2', 'errors' => 'Tag not current user']);
        }

        $tag->loss = 1;
        $tag->update();
        return response()->json(['result'=>'0']);
    }

    public function postRemovedeclareTag(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();
        $validator = Validator::make($request->all(), [
            'serial' => 'required|min:4|max:62'
        ]);

        if ($validator->fails()) {
            return response()->json(['result'=>2,'errors' => $validator->errors()]);
        }

        if (!$tag = Tag::where('serial',$request["serial"])->first()) {
            return response()->json(['result' => 2, 'errors' => 'Tag not found']);
        }
        if (!$tag['user_id']==$user['id']){
            return response()->json(['result' => 2, 'errors' => 'Tag not current user']);
        }

        $tag->loss = 0;
        $tag->update();
        return response()->json(['result'=>0]);
    }

    public function postCheckTag(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();
        $validator = Validator::make($request->all(), [
            'serial' => 'required|min:4|max:62'
        ]);

        if ($validator->fails()) {
            return response()->json(['result'=>2,'errors' => $validator->errors()]);
        }

        if (!$tag = Tag::where('serial',$request["serial"])->first()) {
            return response()->json(['result' => 2, 'errors' => 'Tag not found']);
        }
        if ($tag['loss']=1){
            return response()->json(['result' => 0, 'localize' => 1]);
        }
        return response()->json(['result' => 0, 'localize' => 0]);
    }



    public function postLocalizeTag(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();
        $validator = Validator::make($request->all(), [
            'serial'     => 'required|min:4|max:62',
            'latitude'   => 'required',
            'longitude'   => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['result'=>2,'errors' => $validator->errors()]);
        }

        if (!$tag = Tag::where('serial',$request["serial"])->first()) {
            return response()->json(['result' => 2, 'errors' => 'Tag not found']);
        }

        $tag->latitude = $request["latitude"];
        $tag->longitude= $request["longitude"];
        $tag->update();
        return response()->json(['result'=>0]);
    }



    public function postDeleteTag(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::parseToken()->authenticate();
        $validator = Validator::make($request->all(), [
            'serial' => 'required|min:4|max:62'
        ]);
        if ($validator->fails()) {
            return response()->json(['result'=>1,'errors' => $validator->errors()]);
        }

        if (!$tag = Tag::where('serial',$request["serial"])->first()) {
            return response()->json(['result' => 1, 'errors' => 'Tag not found']);
        }else{
            if (!$tag['user_id']==$user['id']){
                return response()->json(['result' =>1, 'errors' => 'Tag not current user']);
            }else{
                $tag->user_id=1;
                $tag->update();
                //$tag->delete();
                return response()->json(['result' => 0]);
            }
        }
    }

}
