<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //
    public function postAuthenticate (Request $request)
    {
        $this->validate($request, [
            'email'=>'required',
            'password' => 'required',
            'version' => 'required'
        ]);
        if (Auth::attempt(['email' => $request["email"], 'password' => $request["password"], 'version' => $request["version"]])) {
            return 1;
        }
        return 0;
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
    public function postCreateAccount(Request $request)
    {
        /*$this->validate($request, [
            'email' => 'required|unique:users|email|min:4|max:62',
            'password' => 'required|min:6|max:20|confirmed',
            'firstname' => 'required|min:3|max:50',
            'lastname' => 'required|min:3|max:50',
            'role_id' => 'required|exists:roles,id',
            'version' => 'required'
        ]);*/
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|email|min:4|max:62',
            'password' => 'required|min:6|max:20|confirmed',
            'firstname' => 'required|min:3|max:50',
            'lastname' => 'required|min:3|max:50',
            'role_id' => 'required|exists:roles,id',
            'version' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        $request["password"] = bcrypt($request["password"]);
        $user = User::create($request->only([
            'email',
            'password',
            'firstname',
            'lastname',
            'role_id',
            'version'
        ]));
        return redirect()->back()->with(['mensaje' => 'Registro exitoso!', 'user' => $user]);
    }

    public function postUpdateUser(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|min:4|max:62',
            'password' => 'required|min:6|max:20|confirmed',
            'firstname' => 'required|min:3|max:50',
            'lastname' => 'required|min:3|max:50',
            'role_id' => 'required|exists:roles,id',
            'version' => 'required'
        ]);
        $user = User::find($request["id"]);
        $user->email = $request["email"];
        $user->password = $request["password"];
        $user['firstname'] = $request["firstname"];
        $user['lastname'] = $request["lastname"];
        $user->role_id = $request["role_id"];
        $user->version = $request["version"];
        $user->update();
        return redirect()->back()->with(['mensaje' => 'Actualizaci�n exitosa!', 'user' => $user]);
    }

    public function getDeleteUser($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with(['mensaje' => 'Eliminaci�n Exitosa!', 'user' => $user]);
    }
}
