<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;

class EmailController extends Controller
{
    public function postSend(Request $request){
        $title = 'Test App';//$request->input('title');
        $content = 'Hi Jérémy, it is a test sending from app, please resend this email to me (sistemasefrain@gmail.com) to verify that was sending ';//$request->input('content');

       Mail::send('emails.sendpassword', ['title' => $title, 'content' => $content], function ($message)
        {

            $message->from('test@smarttoutou.com', 'Test App');

            $message->to('sistemasefrain@gmail.com');

        });

       /* Mail::send('emails.sendpassword',  ['title' => $title, 'content' => $content], function($message)
        {
            $message->to('sistemasefrain@gmail.com', 'Sender Name')->subject('Welcome!');
        });*/
        return response()->json(['message' => 'Request completed']);
    }

    public function postResetPassword(Request $request){


        if (!$user = User::where('email',$request["email"])->first()) {
            return response()->json(['result' => 1, 'errors' => 'user not found']);
        }else{
            $senderemail=$request["email"];
            $username=$user->firstname . ' '.$user->lastname;
            $newpassword= str_random(20);
            $user->password= bcrypt($newpassword);
            $user->update();
            }





        Mail::send('emails.sendpassword', ['username' => $username, 'newpassword' => $newpassword], function ($message) use ($senderemail)
        {

            $message->from('test@smarttoutou.com', 'SmartToutou');
            $message->subject('Reset Password');

            $message->to($senderemail);

        });

        /* Mail::send('emails.sendpassword',  ['title' => $title, 'content' => $content], function($message)
         {
             $message->to('sistemasefrain@gmail.com', 'Sender Name')->subject('Welcome!');
         });*/
        return response()->json(['result' => 0]);
    }
}
