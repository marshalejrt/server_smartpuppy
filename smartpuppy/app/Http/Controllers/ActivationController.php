<?php

namespace App\Http\Controllers;

use App\Activation;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;


class ActivationController extends Controller
{
    public function postAddCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required'//,
        ]);

        if ($validator->fails()) {
            return response()->json(['result'=>'2','errors' => $validator->errors()]);
        }


        $activation = Activation::create($request->only([
            'code'
        ]));

        return response()->json(['result' => 0]);

    }
}
