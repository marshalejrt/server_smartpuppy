<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $fillable = [ 'serial',
        'name',
        'latitude',
        'longitude',
        'user_id',
        'activate',
        'code'];
}
