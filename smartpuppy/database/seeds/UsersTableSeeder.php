<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = [
            [
                'firstname' => 'Admin',
                'lastname' => 'Sithon Technologies',
                'email' => 'admin@sithon.com',
                'password' => bcrypt('123123'),
                'version' => '1.1.1',
                'role_id' => '1'
            ]
        ];
        foreach ($users as $user) {
            \App\User::create($user);
        }
    }
}
