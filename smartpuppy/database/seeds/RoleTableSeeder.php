<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = [
            [
                'name' => 'Admin',
                'description' => 'Usuarios administradores'
            ],
            [
                'name' => 'Clients',
                'description' => 'Clientes'
            ]
        ];
        foreach ($roles as $role) {
            \App\Role::create($role);
        }
    }
}
