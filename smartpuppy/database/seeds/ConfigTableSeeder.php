<?php

use Illuminate\Database\Seeder;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $configs = [
            [
                'intversion' => 100,
                'stringversion' => '1.0.0'
            ]
        ];
        foreach ($configs as $config) {
            App\Config::create($config);
        }
    }
}
